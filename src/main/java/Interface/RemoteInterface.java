package Interface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RemoteInterface extends Remote {

    // A method to register the user with their username and password. This method will be used to create a new account.
    boolean register(String username, String password) throws RemoteException;

    // A method to authenticate the user with their username and password. This method will be used to allow the user to log in to the application.
    boolean login(String username, String password) throws RemoteException;

    // A method to record the time in. This method will be used to log the time in of the employee.
    boolean timeIn() throws RemoteException;

    // A method to record the time out. This method will be used to log the time out of the employee.
    boolean timeOut() throws RemoteException;

    // A method to retrieve the daily time records of the employee. This method will be used to show the daily time records with the following columns:
    // Date, Time in, Time out.
    List<String[]> getSummary() throws RemoteException;

    // A method to retrieve the current status of the employee (either working or on break).
    // This method will be used to determine the status of the employee and enable/disable the time in/out button.
    String getCurrentStatus() throws RemoteException;
}
